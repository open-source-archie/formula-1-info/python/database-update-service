import requests
from typing import List


def request(method, base_url, end_point, headers: dict = None, params=None):
    """Perform a request to API. If 'GET' request use params for json."""
    if params is None:
        params = {}

    kwargs = {"url": base_url + end_point, "headers": headers}

    if method in ["POST", "PATCH", "PUT"]:
        kwargs["json"] = params
    else:
        kwargs["params"] = params

    try:
        response = requests.request(method, timeout=1, **kwargs)
        response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        raise e

    return response.json()


class ErgastF1Client:
    def __init__(self, ergast_f1_config):
        self.base_uri = ergast_f1_config.base_uri
        self.drivers_endpoint = ergast_f1_config.drivers_endpoint
        self.drivers_standings_endpoint = ergast_f1_config.driver_standings_endpoint
        self.constructors_standings_endpoint = (
            ergast_f1_config.constructor_standings_endpoint
        )

    def get_drivers_standings(self, year="current"):
        """Perform get request for drivers standings. Default year is the current season."""
        response = request(
            "GET", self.base_uri, (year + self.drivers_standings_endpoint)
        )
        return response["MRData"]["StandingsTable"]["StandingsLists"][0][
            "DriverStandings"
        ]


class DatabaseApiClient:
    def __init__(self, web_service_config):
        self.base_url = web_service_config.base_url
        self.driver_standings_endpoint = web_service_config.driver_standings_endpoint

    def post_drivers_standings(self, json_array: List):
        """Performs post requests to the drivers-standings endpoint"""
        response = request(
            "POST", self.base_url, self.driver_standings_endpoint, params=json_array
        )
        return response.json()