import time
from datetime import datetime
from typing import List
import requests_cache

from api_clients import DatabaseApiClient, ErgastF1Client
from config_handler import load_config


class DatabaseUpdateService:
    def __init__(self, ergast_api_client, database_api_client):
        self.ergast_api_client = ergast_api_client
        self.database_api_client = database_api_client

    def run_service(self):
        # Get data from external API
        driver_standings: List = self.ergast_api_client.get_drivers_standings()
        # Post data to webservice API
        self.database_api_client.post_drivers_standings(driver_standings)

        is_updated = False
        while True:
            if current_time() == "9:00" or "15:00" and is_updated is False:
                # Get data from external API
                driver_standings = self.ergast_api_client.get_drivers_standings()
                # Post data to webservice API
                self.database_api_client.post_drivers_standings(driver_standings)

                is_updated = True
            else:
                is_updated = False
                time.sleep(30)


def current_time(strf_format: str = "%H:%M") -> str:
    """ Returns the current time as a string """
    return datetime.strftime(datetime.now(), strf_format)


if __name__ == "__main__":
    config = load_config()

    # Install requests cache
    requests_cache.install_cache(
        cache_name="api_cache", backend="sqlite", expire_after=(60 * 60 * 6)
    )

    # Construct Clients
    database_api_client_instance = DatabaseApiClient(
        config.web_service_api_client_config
    )
    ergast_api_client_instance = ErgastF1Client(config.ergast_f1_client)

    # Construct service
    service = DatabaseUpdateService(
        ergast_api_client_instance, database_api_client_instance
    )

    service.run_service()
