import configparser
from dataclasses import dataclass

from pydantic import BaseModel, parse_obj_as


class ErgastF1ClientConfig(BaseModel):
    base_uri: str
    drivers_endpoint: str
    driver_standings_endpoint: str
    constructor_standings_endpoint: str


class DatabaseApiClientConfig(BaseModel):
    base_url: str
    driver_standings_endpoint: str


@dataclass
class Config:
    ergast_f1_client: ErgastF1ClientConfig
    web_service_api_client_config: DatabaseApiClientConfig


def load_config_from_ini(config_name: str, config_class):
    """ Loads config from config.ini to specific config class """
    try:
        section = config.items((config_name))
    except configparser.NoSectionError as err:
        raise err

    config_dict = {}
    for name, value in section:
        if not value:
            raise ValueError(
                f"{name.upper()} variable not present for {config_name} in config.ini"
            )
        config_dict[name] = value

    return parse_obj_as(config_class, config_dict)


def load_config() -> Config:
    """ Loads the configs from config.ini to Config class """

    ergast_f1_client_config = load_config_from_ini(
        "ErgastF1ClientConfig", ErgastF1ClientConfig
    )
    database_api_client_config = load_config_from_ini(
        "WebServiceApiClientConfig", DatabaseApiClientConfig
    )

    return Config(ergast_f1_client_config, database_api_client_config)


# Load config from directory
config = configparser.ConfigParser()
try:
    config.read("config.ini")
except FileNotFoundError as fnf_error:
    print(fnf_error)
